1. This folder contains the SVT perspective dataset rectified using the method published with the article

Wajahat hussain, Javier Civera, Luis Montano, Martial Hebert, "Dealing with small data and training blind spots in the Manhattan world", WACV 2016.

2. demo_file.m relates the rectified image with the perspective image.

3. See the rectification code at: https://bitbucket.org/nonsemantic/novel_views/get/master.zip
 
4. Contact:

For any questions, I can be reached via email at wajih.1234@gmail.com
  

References:

1. Wajahat hussain, Javier Civera, Luis Montano, Martial Hebert, "Dealing with small data and training blind spots in the Manhattan world", WACV 2016.

2. David F Fouhey, Wajahat Hussain, Abhinav Gupta, Martial Hebert, "Single Image 3D With No 3D Single Image", ICCV 2015.

3. Versha Hedau, Derek Hoiem, David Forsyth, "Recovering the spatial layout of cluttered rooms", ICCV 2009.

4. T.Q. Phan, P. Shivakumara, S. Tian, C.L. Tan, "Recognising Text with perpsective distortion in natural scenes ICCV 2013.
