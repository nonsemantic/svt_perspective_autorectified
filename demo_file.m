% This is a demo function to show the rectified image and the perspective
% image and the homography relating them.

% SVT persptive dataset (ICCV 2013) has been rectified auto using the code
% https://bitbucket.org/nonsemantic/novel_views/get/master.zip
% See:  Wajahat hussain, Javier Civera, Luis Montano, Martial Hebert, "Dealing with small data and training blind spots in the Manhattan world", WACV 2016.

clc
clear all
close all

svtdir = './svt_perspective_sample/'; % Only single image added
rectimgdir = './Images/'; % rectified images
homodir = './data/';

imgname = '17_03.jpg';

% get perspective image
img = imread(fullfile(svtdir,imgname));

% get  one part of the scene
partname = '_left_1';

% get rectified image
rimgname = fullfile(rectimgdir,[imgname(1:end-4) partname '.jpg']);
rimg =imread(rimgname);

% get rectification homography
homofile = fullfile(homodir,[imgname(1:end-4) partname '.mat']);
load(homofile,'Homo_rect','newT');
% pt2 = inv(Homo_rect)*newT*pt1
% pt2 is in unrectified image
% pt1 in rectified image


% selected quadrilateral on the rectified image
h1=figure;
imshow(img);

h2 = figure;
imshow(rimg); hold on;
% [x,y] = ginput(4);
x = [386.8413 647.9475 665.9548 368.8339]'; 
y = [735.0488 738.0501 955.6386 964.6422]';

plot([x' x(1)],[y' y(1)],'r-','LineWidth',4);

% tranfer quadrilateral on the normal image
xyt=inv(Homo_rect)*newT*[x'; y'; ones(1,4)];
xyt(1,:) = xyt(1,:)./xyt(3,:);
xyt(2,:) = xyt(2,:)./xyt(3,:);
figure(h1);
hold on;
plot([xyt(1,:) xyt(1,1)],[xyt(2,:) xyt(2,1)],'r-','Linewidth',4);
